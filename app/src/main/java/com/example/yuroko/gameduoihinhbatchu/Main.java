package com.example.yuroko.gameduoihinhbatchu;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Main extends AppCompatActivity implements View.OnClickListener {

    private TextView txtheart;
    private TextView txtcoin;
    private Button btnnextquestion;
    private List<Integer> buttonAnswer;
    private String chuoi;
    private int level = 0;
    private int heart = 5;
    private int coin = 0;
    private int leghList;
    private TextView tvSucces;
    private String strResult;
    private int[] buttonChoose = {R.id.btncam1, R.id.btncam2, R.id.btncam3, R.id.btncam4, R.id.btncam5, R.id.btncam6, R.id.btncam7, R.id.btncam8,
            R.id.btncam9, R.id.btncam10, R.id.btncam11, R.id.btncam12, R.id.btncam13, R.id.btncam14, R.id.btncam15, R.id.btncam16};
    private int leng;
    private List<Integer> btnVisibles;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_screen);
        if (btnVisibles == null) {
            btnVisibles = new ArrayList<>();
        }
        initsData();
        initsView();
        showbtnxam(level);
        initsImage(level);
        initsString();
        showbtncam();

        for (Integer button : buttonAnswer
                ) {
            Button btn = findViewById(button);
            btn.setBackgroundResource(R.drawable.xamm);
        }
    }



    private void initsView() {
        tvSucces = findViewById(R.id.tv_succes);
        tvSucces.setVisibility(View.INVISIBLE);
        txtheart = findViewById(R.id.txtheart);
        txtcoin = findViewById(R.id.txtcoint);
        btnnextquestion = findViewById(R.id.nextquestion);
        btnnextquestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (level == 30) {
                    Toast.makeText(Main.this, "SUCCES", Toast.LENGTH_SHORT).show();
                    return;
                }
                level++;
                showbtnxam(level);
                initsImage(level);
                initsString();
                showbtncam();

                for (Integer button : buttonAnswer
                        ) {
                    Button btn = findViewById(button);
                    btn.setBackgroundResource(R.drawable.ic_tile_hover);
                }
                btnnextquestion.setVisibility(View.INVISIBLE);
            }
        });

        for (Integer in : buttonChoose
                ) {
            findViewById(in).setOnClickListener(this);
        }

    }


    private void showbtncam() {
        List<String> c = initsString();
        for (int i = 0; i < 16; i++) {
            Button btn = findViewById(buttonChoose[i]);
            btn.setVisibility(View.VISIBLE);
            btn.setText(c.get(i));
        }
    }

    private List<String> initsString() {
        List<String> str = new ArrayList<>();
        for (int i = 0; i < leng; i++) {
            str.add(chuoi.substring(i, i + 1));
        }
        for (int i = 0; i < 16 - leng; i++) {
            Random random = new Random();
            int v = random.nextInt(22);
            str.add(Data.AB[v]);
        }
        Collections.shuffle(str);
        return str;
    }

    private void initsImage(int index) {
        findViewById(R.id.main_image).setBackgroundResource(Data.IMAGE[index]);
    }

    private void initsData() {
        buttonAnswer = new ArrayList<>();
        buttonAnswer.add(R.id.btnxam1);
        buttonAnswer.add(R.id.btnxam2);
        buttonAnswer.add(R.id.btnxam3);
        buttonAnswer.add(R.id.btnxam4);
        buttonAnswer.add(R.id.btnxam5);
        buttonAnswer.add(R.id.btnxam6);
        buttonAnswer.add(R.id.btnxam7);
        buttonAnswer.add(R.id.btnxam8);
        buttonAnswer.add(R.id.btnxam9);
        buttonAnswer.add(R.id.btnxam10);
        buttonAnswer.add(R.id.btnxam11);
        buttonAnswer.add(R.id.btnxam12);
        buttonAnswer.add(R.id.btnxam13);
        buttonAnswer.add(R.id.btnxam14);
        buttonAnswer.add(R.id.btnxam15);
        buttonAnswer.add(R.id.btnxam16);
        leghList = buttonAnswer.size();
    }

    private void showbtnxam(int index) {
        chuoi = Data.ANSWERS[index];
        leng = Data.ANSWERS[index].length();

        for (int i = 0; i < leng; i++) {
            Button btn = findViewById(buttonAnswer.get(i));
            btn.setText("");
            btn.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View view) {
        for (Integer button : buttonAnswer
                ) {
            Button btn = findViewById(button);
            btn.setBackgroundResource(R.drawable.xamm);
        }
        for (Integer id : buttonChoose
                ) {
            if (view.getId() == id) {
                Button btn = view.findViewById(id);
                strResult = btn.getText().toString();
                for (int i = 0; i < leng; i++
                        ) {
                    Button btnAnswer = findViewById(buttonAnswer.get(i));

                    btnAnswer.setText(strResult);
                    buttonAnswer.remove(i);
                    break;
                }
                btnVisibles.add(id);
                btn.setVisibility(View.INVISIBLE);
                checkAnswer();
            }
        }

    }

    private void checkAnswer() {
        if (buttonAnswer.size() == (leghList - leng)) {
            initsData();
        }
        Button btnFinal = findViewById(buttonAnswer.get(leng - 1));
        if (!btnFinal.getText().toString().equals("")) {
            String answer = "";
            for (Integer button : buttonAnswer
                    ) {
                Button btn = findViewById(button);
                answer += btn.getText();
            }
            Log.d("TAG", "answer: " + answer);
            if (answer.equalsIgnoreCase(chuoi)) {

                for (Integer button : buttonAnswer
                        ) {
                    Button btn = findViewById(button);
                    btn.setBackgroundResource(R.drawable.ic_tile_true);
                }
                coin += 100;
                txtcoin.setText(coin + "");
                Toast.makeText(this, "Answer True", Toast.LENGTH_SHORT).show();
                tvSucces.setVisibility(View.VISIBLE);
                btnnextquestion.setVisibility(View.VISIBLE);
            } else {
                if (heart != 0) {
                    heart--;

                    for (Integer button : buttonAnswer
                            ) {
                        Button btn = findViewById(button);
                        btn.setBackgroundResource(R.drawable.ic_tile_false);
                    }
                    Toast.makeText(this, "Answer Fail", Toast.LENGTH_SHORT).show();
                    showbtnxam(level);
                    initsImage(level);
                    initsString();
                    showbtncam();
                    txtheart.setText(heart + "");
                    showbtnxam(level);
                    tvSucces.setText("Bạn đã chọn đáp án sai");
                    tvSucces.setVisibility(View.VISIBLE);

                    return;
                }
                Toast.makeText(this, "GAME OVER", Toast.LENGTH_SHORT).show();

            }
        }
    }

}
